# translation of kcmkwinrules.po to esperanto
# Copyright (C) 2003 Free Software Foundation, Inc.
# This file is distributed under the same license as the kwin package.
# Cindy McKee <cfmckee@gmail.com>, 2007.
# Pierre-Marie Pédrot <pedrotpmx@wanadoo.fr>, 2008.
# Oliver Kellogg <okellogg@users.sourceforge.net>, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: kcmkwinrules\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-06-07 02:19+0000\n"
"PO-Revision-Date: 2023-06-03 14:21+0100\n"
"Last-Translator: Oliver Kellogg <okellogg@users.sourceforge.net>\n"
"Language-Team: esperanto <kde-i18n-eo@kde.org>\n"
"Language: eo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: translate-po (https://github.com/zcribe/translate-po)\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: kcmrules.cpp:226
#, kde-format
msgid "Copy of %1"
msgstr "Kopio de %1"

#: kcmrules.cpp:406
#, kde-format
msgid "Application settings for %1"
msgstr "Aplikaĵaj agordoj por %1"

#: kcmrules.cpp:428 rulesmodel.cpp:215
#, kde-format
msgid "Window settings for %1"
msgstr "Fenestraj agordoj por %1"

#: optionsmodel.cpp:198
#, kde-format
msgid "Unimportant"
msgstr "Negrava"

#: optionsmodel.cpp:199
#, kde-format
msgid "Exact Match"
msgstr "Ĝusta kongruo"

#: optionsmodel.cpp:200
#, kde-format
msgid "Substring Match"
msgstr "Subĉena kongruo"

#: optionsmodel.cpp:201
#, kde-format
msgid "Regular Expression"
msgstr "Regulesprimo"

#: optionsmodel.cpp:205
#, kde-format
msgid "Apply Initially"
msgstr "Apliki komence"

#: optionsmodel.cpp:206
#, kde-format
msgid ""
"The window property will be only set to the given value after the window is "
"created.\n"
"No further changes will be affected."
msgstr ""
"La fenestro-propreco estos agordita nur al la donita valoro post kiam la "
"fenestro estas kreita.\n"
"Neniuj pluaj ŝanĝoj estos efikigataj."

#: optionsmodel.cpp:209
#, kde-format
msgid "Apply Now"
msgstr "Apliki tuj"

#: optionsmodel.cpp:210
#, kde-format
msgid ""
"The window property will be set to the given value immediately and will not "
"be affected later\n"
"(this action will be deleted afterwards)."
msgstr ""
"La fenestra propreco estos agordita al la donita valoro tuj kaj ne estos "
"tuŝita poste\n"
"(ĉi tiu ago estos forigita poste)."

#: optionsmodel.cpp:213
#, kde-format
msgid "Remember"
msgstr "Memori"

#: optionsmodel.cpp:214
#, kde-format
msgid ""
"The value of the window property will be remembered and, every time the "
"window is created, the last remembered value will be applied."
msgstr ""
"La valoro de la fenestra propreco estos memorita kaj, ĉiufoje kiam la "
"fenestro estas kreita, la lasta memorita valoro estos aplikita."

#: optionsmodel.cpp:217
#, kde-format
msgid "Do Not Affect"
msgstr "Ne influi"

#: optionsmodel.cpp:218
#, kde-format
msgid ""
"The window property will not be affected and therefore the default handling "
"for it will be used.\n"
"Specifying this will block more generic window settings from taking effect."
msgstr ""
"La fenestra propreco ne estos tuŝita kaj tial la defaŭlta uzado por ĝi estos "
"uzata.\n"
"Specifi ĉi tion blokos pli ĝeneralajn fenestrajn agordojn de efektiviĝo."

#: optionsmodel.cpp:221
#, kde-format
msgid "Force"
msgstr "Devigi"

#: optionsmodel.cpp:222
#, kde-format
msgid "The window property will be always forced to the given value."
msgstr "La fenestro-propreco ĉiam estos devigita al la donita valoro."

#: optionsmodel.cpp:224
#, kde-format
msgid "Force Temporarily"
msgstr "Devigi portempe"

#: optionsmodel.cpp:225
#, kde-format
msgid ""
"The window property will be forced to the given value until it is hidden\n"
"(this action will be deleted after the window is hidden)."
msgstr ""
"La fenestro-propreco estos devigita al la donita valoro ĝis ĝi estas kaŝita\n"
"(ĉi tiu ago estos forigita post kiam la fenestro estas kaŝita)."

#: rulesmodel.cpp:218
#, kde-format
msgid "Settings for %1"
msgstr "Agordoj por %1"

#: rulesmodel.cpp:221
#, kde-format
msgid "New window settings"
msgstr "Novaj fenestraj agordoj"

#: rulesmodel.cpp:237
#, kde-format
msgid ""
"You have specified the window class as unimportant.\n"
"This means the settings will possibly apply to windows from all "
"applications. If you really want to create a generic setting, it is "
"recommended you at least limit the window types to avoid special window "
"types."
msgstr ""
"Vi difinis la fenestran klason kiel negravan. \n"
"Ĉi tiu signifas ke la agordoj eble aplikiĝos al fenestroj de ĉiuj aplikaĵoj. "
"Se vi vere intencas krei normalan agordon, estas rekomendita ke vi almenaŭ "
"limigu la specojn de fenestroj por eviti apartajn specojn de fenestroj."

#: rulesmodel.cpp:244
#, kde-format
msgid ""
"Some applications set their own geometry after starting, overriding your "
"initial settings for size and position. To enforce these settings, also "
"force the property \"%1\" to \"Yes\"."
msgstr ""
"Iuj aplikaĵoj starigas sian propran geometrion post ekfunkciigo, "
"superregante viajn komencajn agordojn por grandeco kaj pozicio. Por plenumi "
"ĉi tiujn agordojn, ankaŭ devigu la trajton \"%1\" al \"Jes\"."

#: rulesmodel.cpp:251
#, kde-format
msgid ""
"Readability may be impaired with extremely low opacity values. At 0%, the "
"window becomes invisible."
msgstr ""
"Legeblo povus difektiĝi je ekstreme malaltaj valoroj de opakeco. Je 0%, la "
"fenestro iĝas nevidebla."

#: rulesmodel.cpp:382
#, kde-format
msgid "Description"
msgstr "Priskribo"

#: rulesmodel.cpp:382 rulesmodel.cpp:390 rulesmodel.cpp:398 rulesmodel.cpp:405
#: rulesmodel.cpp:411 rulesmodel.cpp:419 rulesmodel.cpp:424 rulesmodel.cpp:430
#, kde-format
msgid "Window matching"
msgstr "Fenestra kongruo"

#: rulesmodel.cpp:390
#, kde-format
msgid "Window class (application)"
msgstr "Fenestra klaso (aplikaĵo)"

#: rulesmodel.cpp:398
#, kde-format
msgid "Match whole window class"
msgstr "Kongrui la tutan fenestroklason"

#: rulesmodel.cpp:405
#, kde-format
msgid "Whole window class"
msgstr "Tuta fenestroklaso"

#: rulesmodel.cpp:411
#, kde-format
msgid "Window types"
msgstr "Fenestroj"

#: rulesmodel.cpp:419
#, kde-format
msgid "Window role"
msgstr "Fenestra rolo"

#: rulesmodel.cpp:424
#, kde-format
msgid "Window title"
msgstr "Fenestra titolo"

#: rulesmodel.cpp:430
#, kde-format
msgid "Machine (hostname)"
msgstr "Maŝino (gastigantonomo)"

#: rulesmodel.cpp:436
#, kde-format
msgid "Position"
msgstr "Pozicio"

#: rulesmodel.cpp:436 rulesmodel.cpp:442 rulesmodel.cpp:448 rulesmodel.cpp:453
#: rulesmodel.cpp:461 rulesmodel.cpp:467 rulesmodel.cpp:486 rulesmodel.cpp:502
#: rulesmodel.cpp:507 rulesmodel.cpp:512 rulesmodel.cpp:517 rulesmodel.cpp:522
#: rulesmodel.cpp:531 rulesmodel.cpp:546 rulesmodel.cpp:551 rulesmodel.cpp:556
#, kde-format
msgid "Size & Position"
msgstr "Grandeco & Pozicio"

#: rulesmodel.cpp:442
#, kde-format
msgid "Size"
msgstr "Grandeco"

#: rulesmodel.cpp:448
#, kde-format
msgid "Maximized horizontally"
msgstr "Maksimumigita horizontale"

#: rulesmodel.cpp:453
#, kde-format
msgid "Maximized vertically"
msgstr "Maksimumigita vertikale"

#: rulesmodel.cpp:461
#, kde-format
msgid "Virtual Desktop"
msgstr "Virtuala Labortablo"

#: rulesmodel.cpp:467
#, kde-format
msgid "Virtual Desktops"
msgstr "Virtualaj Labortabloj"

#: rulesmodel.cpp:486
#, kde-format
msgid "Activities"
msgstr "Aktivecoj"

#: rulesmodel.cpp:502
#, kde-format
msgid "Screen"
msgstr "Ekrano"

#: rulesmodel.cpp:507
#, kde-format
msgid "Fullscreen"
msgstr "Plenekrane"

#: rulesmodel.cpp:512
#, kde-format
msgid "Minimized"
msgstr "Minimumigita"

#: rulesmodel.cpp:517
#, kde-format
msgid "Shaded"
msgstr "Ombrita"

#: rulesmodel.cpp:522
#, kde-format
msgid "Initial placement"
msgstr "Komenca lokigo"

#: rulesmodel.cpp:531
#, kde-format
msgid "Ignore requested geometry"
msgstr "Ignori petitan geometrion"

#: rulesmodel.cpp:534
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"Some applications can set their own geometry, overriding the window manager "
"preferences. Setting this property overrides their placement requests.<nl/"
"><nl/>This affects <interface>Size</interface> and <interface>Position</"
"interface> but not <interface>Maximized</interface> or "
"<interface>Fullscreen</interface> states.<nl/><nl/>Note that the position "
"can also be used to map to a different <interface>Screen</interface>"
msgstr ""
"Iuj aplikaĵoj povas agordi sian propran geometrion, superregante la "
"preferojn de la fenestromanaĝero. Agordi ĉi tiun proprecon superregas iliajn "
"lokigajn petojn.<nl/><nl/>Tio influas <interface>Grandeco</interface> kaj "
"<interface>Pozicion</interface> sed ne <interface>Maksimumigita</interface> "
"aŭ <interface>Plena ekrano. </interface> deklaras.<nl/><nl/>Rimarku, ke la "
"pozicio ankaŭ povas esti uzata por mapi al malsama <interface>Ekrano</"
"interface>"

#: rulesmodel.cpp:546
#, kde-format
msgid "Minimum Size"
msgstr "Minimuma Grandeco"

#: rulesmodel.cpp:551
#, kde-format
msgid "Maximum Size"
msgstr "Maksimuma Grandeco"

#: rulesmodel.cpp:556
#, kde-format
msgid "Obey geometry restrictions"
msgstr "Obei geometriajn restriktojn"

#: rulesmodel.cpp:558
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"Some apps like video players or terminals can ask KWin to constrain them to "
"certain aspect ratios or only grow by values larger than the dimensions of "
"one character. Use this property to ignore such restrictions and allow those "
"windows to be resized to arbitrary sizes.<nl/><nl/>This can be helpful for "
"windows that can't quite fit the full screen area when maximized."
msgstr ""
"Iuj aplikoj kiel videoludiloj aŭ terminaloj povas peti al KWin limigi ilin "
"al certa bildformato aŭ nur kreski per valoroj pli grandaj ol la dimensioj "
"de unu signo. Uzu ĉi tiun proprecon por ignori tiajn limigojn kaj permesi ke "
"tiuj fenestroj aligrandiĝu al arbitraj grandecoj.<nl/><nl/>Tio povas helpi "
"pri fenestroj kiuj ne entute ĝisrandas la plenekranan areon je maksimumiĝo."

#: rulesmodel.cpp:569
#, kde-format
msgid "Keep above other windows"
msgstr "Teni super aliaj fenestroj"

#: rulesmodel.cpp:569 rulesmodel.cpp:574 rulesmodel.cpp:579 rulesmodel.cpp:585
#: rulesmodel.cpp:591 rulesmodel.cpp:597
#, kde-format
msgid "Arrangement & Access"
msgstr "Aranĝo & Aliro"

#: rulesmodel.cpp:574
#, kde-format
msgid "Keep below other windows"
msgstr "Teni sub aliaj fenestroj"

#: rulesmodel.cpp:579
#, kde-format
msgid "Skip taskbar"
msgstr "Preterpasi taskobreton"

#: rulesmodel.cpp:581
#, kde-format
msgctxt "@info:tooltip"
msgid "Controls whether or not the window appears in the Task Manager."
msgstr "Mastrumas ĉu aŭ ne la fenestro aperas en la Task Manager."

#: rulesmodel.cpp:585
#, kde-format
msgid "Skip pager"
msgstr "Preterpaĝilo"

#: rulesmodel.cpp:587
#, kde-format
msgctxt "@info:tooltip"
msgid ""
"Controls whether or not the window appears in the Virtual Desktop manager."
msgstr ""
"Mastrumas ĉu aŭ ne la fenestro aperas en la Virtuala Labortablo-"
"administranto."

#: rulesmodel.cpp:591
#, kde-format
msgid "Skip switcher"
msgstr "Preterŝaltilo"

#: rulesmodel.cpp:593
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"Controls whether or not the window appears in the <shortcut>Alt+Tab</"
"shortcut> window list."
msgstr ""
"Mastrumas ĉu aŭ ne la fenestro aperas en la fenestrolisto <shortcut>Alt+Tab</"
"shortcut>."

#: rulesmodel.cpp:597
#, kde-format
msgid "Shortcut"
msgstr "Fulmoklavo"

#: rulesmodel.cpp:603
#, kde-format
msgid "No titlebar and frame"
msgstr "Neniu titolbreto kaj kadro"

#: rulesmodel.cpp:603 rulesmodel.cpp:608 rulesmodel.cpp:614 rulesmodel.cpp:619
#: rulesmodel.cpp:625 rulesmodel.cpp:652 rulesmodel.cpp:680 rulesmodel.cpp:686
#: rulesmodel.cpp:698 rulesmodel.cpp:703 rulesmodel.cpp:709 rulesmodel.cpp:714
#, kde-format
msgid "Appearance & Fixes"
msgstr "Apero kaj Korektoj"

#: rulesmodel.cpp:608
#, kde-format
msgid "Titlebar color scheme"
msgstr "Titolbreto kolorskemo"

#: rulesmodel.cpp:614
#, kde-format
msgid "Active opacity"
msgstr "Aktiva opakeco"

#: rulesmodel.cpp:619
#, kde-format
msgid "Inactive opacity"
msgstr "Neaktiva opakeco"

#: rulesmodel.cpp:625
#, kde-format
msgid "Focus stealing prevention"
msgstr "Fokus-ŝtela malhelpo"

#: rulesmodel.cpp:627
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"KWin tries to prevent windows that were opened without direct user action "
"from raising themselves and taking focus while you're currently interacting "
"with another window. This property can be used to change the level of focus "
"stealing prevention applied to individual windows and apps.<nl/><nl/>Here's "
"what will happen to a window opened without your direct action at each level "
"of focus stealing prevention:<nl/><list><item><emphasis strong='true'>None:</"
"emphasis> The window will be raised and focused.</item><item><emphasis "
"strong='true'>Low:</emphasis> Focus stealing prevention will be applied, but "
"in the case of a situation KWin considers ambiguous, the window will be "
"raised and focused.</item><item><emphasis strong='true'>Normal:</emphasis> "
"Focus stealing prevention will be applied, but  in the case of a situation "
"KWin considers ambiguous, the window will <emphasis>not</emphasis> be raised "
"and focused.</item><item><emphasis strong='true'>High:</emphasis> The window "
"will only be raised and focused if it belongs to the same app as the "
"currently-focused window.</item><item><emphasis strong='true'>Extreme:</"
"emphasis> The window will never be raised and focused.</item></list>"
msgstr ""
"KWin provas malhelpi fenestrojn kiuj estis malfermitaj sen rekta uzantago "
"levi sin kaj fokusigi dum vi nuntempe interagas kun alia fenestro. Ĉi tiu "
"propreco povas esti uzata por ŝanĝi la nivelon de fokusa ŝtelado-preventado "
"aplikita al unuopaj fenestroj kaj aplikaĵoj.<nl/><nl/>Jen kio okazos al "
"fenestro malfermita sen via rekta ago ĉe ĉiu nivelo de fokusa ŝtelado-"
"preventado:<nl/><list><item><emphasis strong='true'>Nenio:</emphasis> La "
"fenestro estos levita kaj fokusita.</item><item><emphasis "
"strong='true'>Malalta:</emphasis> Preventado de fokusa ŝtelado estos "
"aplikita, sed en la kazo de situacio kiun KWin konsideras ambigua, la "
"fenestro estos levita kaj fokusita.</item><item><emphasis "
"strong='true'>Normala:</emphasis> Fokusŝtelado. prevento estos aplikata, sed "
"en la kazo de situacio kiun KWin konsideras ambigua, la fenestro "
"<emphasis>ne</emphasis> estos levita kaj fokusita.</item><item><emphasis "
"strong='true'>Alta:</emphasis> La fenestro estos nur levita kaj fokusita se "
"ĝi apartenas al la sama programo kiel la nune fokusita fenestro.</"
"item><item><emphasis strong='true'>Ekstrema:</emphasis> La fenestro neniam "
"estos esti levita kaj koncentrita.</item></list>"

#: rulesmodel.cpp:652
#, kde-format
msgid "Focus protection"
msgstr "Fokusa protekto"

#: rulesmodel.cpp:654
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"This property controls the focus protection level of the currently active "
"window. It is used to override the focus stealing prevention applied to new "
"windows that are opened without your direct action.<nl/><nl/>Here's what "
"happens to new windows that are opened without your direct action at each "
"level of focus protection while the window with this property applied to it "
"has focus:<nl/><list><item><emphasis strong='true'>None</emphasis>: Newly-"
"opened windows always raise themselves and take focus.</item><item><emphasis "
"strong='true'>Low:</emphasis> Focus stealing prevention will be applied to "
"the newly-opened window, but in the case of a situation KWin considers "
"ambiguous, the window will be raised and focused.</item><item><emphasis "
"strong='true'>Normal:</emphasis> Focus stealing prevention will be applied "
"to the newly-opened window, but in the case of a situation KWin considers "
"ambiguous, the window will <emphasis>not</emphasis> be raised and focused.</"
"item><item><emphasis strong='true'>High:</emphasis> Newly-opened windows "
"will only raise themselves and take focus if they belongs to the same app as "
"the currently-focused window.</item><item><emphasis strong='true'>Extreme:</"
"emphasis> Newly-opened windows never raise themselves and take focus.</"
"item></list>"
msgstr ""
"Ĉi tiu propreco kontrolas la fokusan protektonivelon de la nuntempe aktiva "
"fenestro. Ĝi estas uzata por superregi la preventon de fokusa ŝtelado "
"aplikita al novaj fenestroj kiuj estas malfermitaj sen via rekta ago.<nl/"
"><nl/>Jen kio okazas al novaj fenestroj kiuj estas malfermitaj sen via rekta "
"ago je ĉiu nivelo de fokusa protekto dum la fenestro kun ĉi tiu propreco "
"aplikita al ĝi havas fokuson:<nl/><list><item><emphasis strong='true'>Neniu</"
"emphasis>: Nov-malfermitaj fenestroj ĉiam levas sin kaj fokusiĝas.</"
"item><item><emphasis strong='true'>Malalta:</emphasis> Antaŭzorgo pri fokusa "
"ŝtelado estos aplikata al la ĵus malfermita fenestro, sed en la kazo de "
"situacio kiun KWin konsideras ambigua, la fenestro estos levita kaj fokusita."
"</item><item><emphasis strong='true'>Normala:</emphasis> Fokusŝtela prevento "
"estos aplikita al la ĵus malfermita fenestro, sed en la kazo de situacio "
"KWin konsideras ambigua, la fenestro <emphasis>ne </emphasis> estos levita "
"kaj fokusita.</item><item><emphasis strong='true'>Alta:</emphasis> Nov-"
"malfermitaj fenestroj nur leviĝos kaj fokusiĝos se ili apartenas al la sama "
"programo kiel la nune fokusita fenestro.</item><item><emphasis "
"strong='true'>Ekstrema:</emphasis> Nove malfermitaj fenestroj neniam levas "
"sin kaj fokusiĝas.</item></list>"

#: rulesmodel.cpp:680
#, kde-format
msgid "Accept focus"
msgstr "Akcepti fokuson"

#: rulesmodel.cpp:682
#, kde-format
msgid "Controls whether or not the window becomes focused when clicked."
msgstr "Mastrumas ĉu la fenestro fokusiĝas aŭ ne kiam oni klakas."

#: rulesmodel.cpp:686
#, kde-format
msgid "Ignore global shortcuts"
msgstr "Ignori ĉieajn ŝparvojojn"

#: rulesmodel.cpp:688
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"Use this property to prevent global keyboard shortcuts from working while "
"the window is focused. This can be useful for apps like emulators or virtual "
"machines that handle some of the same shortcuts themselves.<nl/><nl/>Note "
"that you won't be able to <shortcut>Alt+Tab</shortcut> out of the window or "
"use any other global shortcuts such as <shortcut>Alt+Space</shortcut> to "
"activate KRunner."
msgstr ""
"Uzu ĉi tiun proprecon por malhelpi ke ĉieaj fulmoklavoj funkciu dum kiam la "
"fenestro estas fokusita. Ĉi tio povas esti utila por aplikaĵoj kiel emuliloj "
"aŭ virtualaj maŝinoj, kiuj mem traktas kelkajn el la samaj ŝparvojoj.<nl/"
"><nl/>Rimarku, ke vi ne povos <shortcut>Alt+Tab</shortcut> eliri fenestron "
"aŭ uzi aliajn ĉieajn ŝparvojojn kiel <shortcut>Alt+Spaco</shortcut> por "
"aktivigi KRunner."

#: rulesmodel.cpp:698
#, kde-format
msgid "Closeable"
msgstr "Fermebla"

#: rulesmodel.cpp:703
#, kde-format
msgid "Set window type"
msgstr "Meti fenestrotipon"

#: rulesmodel.cpp:709
#, kde-format
msgid "Desktop file name"
msgstr "Labortabla dosiernomo"

#: rulesmodel.cpp:714
#, kde-format
msgid "Block compositing"
msgstr "Bloko-komponado"

#: rulesmodel.cpp:766
#, kde-format
msgid "Window class not available"
msgstr "Fenestra klaso ne havebla"

#: rulesmodel.cpp:767
#, kde-kuit-format
msgctxt "@info"
msgid ""
"This application is not providing a class for the window, so KWin cannot use "
"it to match and apply any rules. If you still want to apply some rules to "
"it, try to match other properties like the window title instead.<nl/><nl/"
">Please consider reporting this bug to the application's developers."
msgstr ""
"Ĉi tiu aplikaĵo ne provizas klason por la fenestro, do KWin ne povas uzi ĝin "
"por kongrui kaj apliki iujn ajn regulojn. Se vi ankoraŭ volas apliki iujn "
"regulojn al ĝi, provu kongrui kun aliaj trajtoj kiel la fenestrotitolo "
"anstataŭe.<nl/><nl/>Bonvolu konsideri raporti ĉi tiun cimon al la "
"programistoj de la aplikaĵo."

#: rulesmodel.cpp:801
#, kde-format
msgid "All Window Types"
msgstr "Ĉiuj Fenestroj"

#: rulesmodel.cpp:802
#, kde-format
msgid "Normal Window"
msgstr "Ordinara fenestro"

#: rulesmodel.cpp:803
#, kde-format
msgid "Dialog Window"
msgstr "Dialoga fenestro"

#: rulesmodel.cpp:804
#, kde-format
msgid "Utility Window"
msgstr "Utilaĵa fenestro"

#: rulesmodel.cpp:805
#, kde-format
msgid "Dock (panel)"
msgstr "Doko (panela)"

#: rulesmodel.cpp:806
#, kde-format
msgid "Toolbar"
msgstr "Ilobreto"

#: rulesmodel.cpp:807
#, kde-format
msgid "Torn-Off Menu"
msgstr "Malfiksita menuo"

#: rulesmodel.cpp:808
#, kde-format
msgid "Splash Screen"
msgstr "Salutŝildo"

#: rulesmodel.cpp:809
#, kde-format
msgid "Desktop"
msgstr "Labortablo"

#. i18n("Unmanaged Window")},  deprecated
#: rulesmodel.cpp:811
#, kde-format
msgid "Standalone Menubar"
msgstr "Memstara menuzono"

#: rulesmodel.cpp:812
#, kde-format
msgid "On Screen Display"
msgstr "Sur Ekrana Ekrano"

#: rulesmodel.cpp:822
#, kde-format
msgid "All Desktops"
msgstr "Ĉiuj labortabloj"

#: rulesmodel.cpp:824
#, kde-format
msgctxt "@info:tooltip in the virtual desktop list"
msgid "Make the window available on all desktops"
msgstr "Igi la fenestron havebla sur ĉiuj labortabloj"

#: rulesmodel.cpp:843
#, kde-format
msgid "All Activities"
msgstr "Ĉiuj Agadoj"

#: rulesmodel.cpp:845
#, kde-format
msgctxt "@info:tooltip in the activity list"
msgid "Make the window available on all activities"
msgstr "Disponigi la fenestron pri ĉiuj agadoj"

#: rulesmodel.cpp:866
#, kde-format
msgid "Default"
msgstr "Defaŭlta"

#: rulesmodel.cpp:867
#, kde-format
msgid "No Placement"
msgstr "Nenia metado"

#: rulesmodel.cpp:868
#, kde-format
msgid "Minimal Overlapping"
msgstr "Minimuma Interkovro"

#: rulesmodel.cpp:869
#, kde-format
msgid "Maximized"
msgstr "Maksimumigita"

#: rulesmodel.cpp:870
#, kde-format
msgid "Centered"
msgstr "Centrigita"

#: rulesmodel.cpp:871
#, kde-format
msgid "Random"
msgstr "Hazarde"

#: rulesmodel.cpp:872
#, kde-format
msgid "In Top-Left Corner"
msgstr "En Supra-Maldekstra Angulo"

#: rulesmodel.cpp:873
#, kde-format
msgid "Under Mouse"
msgstr "Sube de muso"

#: rulesmodel.cpp:874
#, kde-format
msgid "On Main Window"
msgstr "Sur ĉefa fenestro"

#: rulesmodel.cpp:881
#, kde-format
msgid "None"
msgstr "Nenia"

#: rulesmodel.cpp:882
#, kde-format
msgid "Low"
msgstr "Malalta"

#: rulesmodel.cpp:883
#, kde-format
msgid "Normal"
msgstr "Normala"

#: rulesmodel.cpp:884
#, kde-format
msgid "High"
msgstr "Alta"

#: rulesmodel.cpp:885
#, kde-format
msgid "Extreme"
msgstr "Ekstrema"

#: rulesmodel.cpp:928
#, kde-format
msgid "Unmanaged window"
msgstr "Neadministrata fenestro"

#: rulesmodel.cpp:929
#, kde-format
msgid "Could not detect window properties. The window is not managed by KWin."
msgstr ""
"Ne eblis detekti fenestropropraĵojn. La fenestro ne estas administrita de "
"KWin."

#: ui/FileDialogLoader.qml:15
#, kde-format
msgid "Select File"
msgstr "Elekti Dosieron"

#: ui/FileDialogLoader.qml:27
#, kde-format
msgid "KWin Rules (*.kwinrule)"
msgstr "KWin Reguloj (*.kwinrule)"

#: ui/main.qml:62
#, kde-format
msgid "No rules for specific windows are currently set"
msgstr "Neniuj reguloj por specifaj fenestroj estas nuntempe fiksitaj"

#: ui/main.qml:63
#, kde-kuit-format
msgctxt "@info"
msgid "Click the <interface>Add New…</interface> button below to add some"
msgstr ""
"Alklaku la suban butonon <interface>Aldoni novan…</interface> por aldoni "
"kelkajn"

#: ui/main.qml:71
#, kde-format
msgid "Select the rules to export"
msgstr "Elekti la regulojn por eksporti"

#: ui/main.qml:75
#, kde-format
msgid "Unselect All"
msgstr "Malelekti Ĉion"

#: ui/main.qml:75
#, kde-format
msgid "Select All"
msgstr "Elekti ĉiujn"

#: ui/main.qml:89
#, kde-format
msgid "Save Rules"
msgstr "Konservi Regulojn"

#: ui/main.qml:100
#, kde-format
msgid "Add New…"
msgstr "Aldoni novan…"

#: ui/main.qml:111
#, kde-format
msgid "Import…"
msgstr "Importi…"

#: ui/main.qml:119
#, kde-format
msgid "Cancel Export"
msgstr "Nuligi Eksporton"

#: ui/main.qml:119
#, kde-format
msgid "Export…"
msgstr "Eksporti…"

#: ui/main.qml:209
#, kde-format
msgid "Edit"
msgstr "Redakti"

#: ui/main.qml:218
#, kde-format
msgid "Duplicate"
msgstr "Duobligi"

#: ui/main.qml:227
#, kde-format
msgid "Delete"
msgstr "Forigi"

#: ui/main.qml:240
#, kde-format
msgid "Import Rules"
msgstr "Reguloj de importo"

#: ui/main.qml:252
#, kde-format
msgid "Export Rules"
msgstr "Eksportaj Reguloj"

#: ui/OptionsComboBox.qml:35
#, kde-format
msgid "None selected"
msgstr "Neniu elektita"

#: ui/OptionsComboBox.qml:41
#, kde-format
msgid "All selected"
msgstr "Ĉiuj elektitaj"

#: ui/OptionsComboBox.qml:43
#, kde-format
msgid "%1 selected"
msgid_plural "%1 selected"
msgstr[0] "%1 elektita"
msgstr[1] "%1 elektitaj"

#: ui/RulesEditor.qml:63
#, kde-format
msgid "No window properties changed"
msgstr "Neniuj fenestraj proprietoj ŝanĝiĝis"

#: ui/RulesEditor.qml:64
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Click the <interface>Add Property...</interface> button below to add some "
"window properties that will be affected by the rule"
msgstr ""
"Alklaku la suban butonon <interface>Aldoni Propraĵon...</interface> por "
"aldoni kelkajn fenestrajn ecojn, kiuj estos tuŝitaj de la regulo."

#: ui/RulesEditor.qml:85
#, kde-format
msgid "Close"
msgstr "Fermi"

#: ui/RulesEditor.qml:85
#, kde-format
msgid "Add Property..."
msgstr "Aldoni Trajton..."

#: ui/RulesEditor.qml:98
#, kde-format
msgid "Detect Window Properties"
msgstr "Detekti Fenestrajn Propraĵojn"

#: ui/RulesEditor.qml:114 ui/RulesEditor.qml:121
#, kde-format
msgid "Instantly"
msgstr "Tuj"

#: ui/RulesEditor.qml:115 ui/RulesEditor.qml:126
#, kde-format
msgid "After %1 second"
msgid_plural "After %1 seconds"
msgstr[0] "Post %1 sekundo"
msgstr[1] "Post %1 sekundoj"

#: ui/RulesEditor.qml:175
#, kde-format
msgid "Add property to the rule"
msgstr "Aldoni trajton al la regulo"

#: ui/RulesEditor.qml:276 ui/ValueEditor.qml:54
#, kde-format
msgid "Yes"
msgstr "Jes"

#: ui/RulesEditor.qml:276 ui/ValueEditor.qml:60
#, kde-format
msgid "No"
msgstr "Ne"

#: ui/RulesEditor.qml:278 ui/ValueEditor.qml:168 ui/ValueEditor.qml:175
#, kde-format
msgid "%1 %"
msgstr "%1 %"

#: ui/RulesEditor.qml:280
#, kde-format
msgctxt "Coordinates (x, y)"
msgid "(%1, %2)"
msgstr "(%1, %2)"

#: ui/RulesEditor.qml:282
#, kde-format
msgctxt "Size (width, height)"
msgid "(%1, %2)"
msgstr "(%1, %2)"

#: ui/ValueEditor.qml:203
#, kde-format
msgctxt "(x, y) coordinates separator in size/position"
msgid "x"
msgstr "x"
